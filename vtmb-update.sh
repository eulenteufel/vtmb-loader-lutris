#!/bin/bash

# If $DOWNLOAD_PATH is not defined, use the script path
[ ! -n "$DOWNLOAD_PATH" ] && DOWNLOAD_PATH=$(dirname "$(readlink -f "$0")") 

cd $DOWNLOAD_PATH

UPDATE_FEED='https://rss.moddb.com/mods/vtmb-unofficial-patch/downloads/feed/rss.xml'

# Get the download id of the newest version
download_id="$(curl "$UPDATE_FEED" \
    | grep -m1 guid \
    | sed -Ee 's/<guid isPermaLink="false">downloads([0-9]+)<\/guid>/\1/g' \
    | tr -d '[:space:]')"

# Geting the file to find the newest Unofficial Patch Download URL
wget "https://www.moddb.com/downloads/start/$download_id/all" -O mirrors.txt
MIRROR=$(grep -m1 'Start' 'mirrors.txt')

# Get the executable filename
FILE_NAME=$(grep -oP '(VTMBup[0-9a-z]*.exe)' <<< $MIRROR)

# Get the download URL
URL="https://www.moddb.com/"$(grep -oP 'downloads(\/[\w]*)+' <<< $MIRROR)

echo "Downloading $FILE_NAME from $URL"

# Download the Unofficial Patch
wget -c --retry-connrefused "$URL" -O vtmbup.exe
