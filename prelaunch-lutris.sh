#!/bin/bash
# Wine Linux loading script for lutris by welblade

# cd to the script's directory
GAME_PATH=$(dirname "$(readlink -f "$0")")
cd "$GAME_PATH"

# go to parent directory, as long as no file Vampire.exe exists in current directory 
while ([ ! -f "./Vampire.exe" ] && [ ! -f "./vampire.exe" ]);
do
	if  [[ "$PWD" == "/" ]]; then
		if [ -t 1 ]; then
			echo "This already is the root directory; giving up to find Vampire.exe. This script must be placed in the Bloodlines directory or one of its subdirectories to work." 1>&2
		else
			zenity --error --text "The script's directory and none of its parent directories contains Vampire.exe. This script must be placed in the Bloodlines directory or one of its subdirectories to work."
		fi
		exit 1
	fi
	echo "Vampire.exe not found in directory. Checking parent directory."
	cd ..
done

echo "Vampire.exe found. Assuming that this is the Bloodlines directory."

#use \n for array seperator - this will break on filenames with \n
IFS="	
"
#find all directories which have a child directory named python and put their basenames in a array
PMODS=( $(find . -maxdepth 1 -type d -exec [ -d "{}/python" ] \; -exec basename "{}" \; ) )


if [ ${#PMODS[@]} -eq 0 ]; then
	zenity --error --text "No mod directories detected (do you have this script in the Bloodlines directory?)"
	exit 1
fi

#only 1 dir, normal game or total conversion
if [ ${#PMODS[@]} -eq 1 ]; then
	SEL="${PMODS[0]}"
else
	#ask the user which one
	SEL=$(zenity --list --column "Available mods" --title "Select mod" --text="More than one possible mod, select Vampire or OK for the original game:" "${PMODS[@]}")
	#user didn't cancel
	if [ $? -ne 0 ]; then
		exit 1
	fi
fi

echo "$SEL" > .vampire_mod
